INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (10, 'Karamat', 'M', 'Developer', 'A');
INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (11, 'Luan', 'M', 'Developer', 'A');
INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (12, 'Stefan', 'V', 'Developer', 'A');
INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (13, 'T', 'B', 'Developer', 'A');
INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (14, 'Mo', 'A', 'Developer', 'A');
INSERT INTO employee (employee_id, first_name, last_name, employee_role, team) VALUES (15, 'Alfredo', 'F', 'Developer', 'A');