package com.example.hrservice.dao;

import com.example.hrservice.model.Employee;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    
}
