package com.example.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeDTO {

    private int employeeId;

    private String firstName;

    private String lastName;

    private String employeeRole;

    private String team;

}