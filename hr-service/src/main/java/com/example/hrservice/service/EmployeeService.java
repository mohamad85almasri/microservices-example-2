package com.example.hrservice.service;

import com.example.hrservice.dto.EmployeeDTO;

public interface EmployeeService {
    
    public EmployeeDTO getEmployee(Integer id);
}
