package com.example.hrservice.service.impl;

import java.util.Optional;

import com.example.hrservice.dao.EmployeeRepository;
import com.example.hrservice.model.Employee;
import com.example.hrservice.dto.EmployeeDTO;
import com.example.hrservice.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeDTO getEmployee(Integer id) {
        Optional<Employee> op = employeeRepository.findById(id);
        if (op.isPresent()) {
            Employee employee = op.get();
    
            
            return new EmployeeDTO(employee.getEmployeeId(), employee.getFirstName(), employee.getLastName(),
                employee.getEmployeeRole(), employee.getTeam());

        }
        return null;
    }

}
