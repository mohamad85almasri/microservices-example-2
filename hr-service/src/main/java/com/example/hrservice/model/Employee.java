package com.example.hrservice.model;


import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;


@Data
@Entity
public class Employee {

    @Id
    private int employeeId;

    private String firstName;

    private String lastName;

    private String employeeRole;

    private String team;

}
