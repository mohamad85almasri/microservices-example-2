package com.example.hrservice.controller;

import com.example.hrservice.dto.EmployeeDTO;
import com.example.hrservice.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employee")
    public EmployeeDTO getEmployeeInfo(@RequestParam("id") Integer id) {
        log.info("Returning Employee Information");
        EmployeeDTO employeeDTO = employeeService.getEmployee(id);

        return employeeDTO;
    }
}
