package com.example.timereportservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class TimeReportServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeReportServiceApplication.class, args);
	}

}
