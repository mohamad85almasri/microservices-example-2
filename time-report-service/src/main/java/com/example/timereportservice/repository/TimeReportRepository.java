package com.example.timereportservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.timereportservice.model.TimeReport;

public interface TimeReportRepository extends JpaRepository<TimeReport, Integer>{

}
