package com.example.timereportservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.timereportservice.model.TimeReport;
import com.example.timereportservice.repository.TimeReportRepository;

import io.swagger.annotations.ApiOperation;

@RequestMapping("/time")
@RestController
@EnableAutoConfiguration
public class TimeReportController {
	@Autowired
	Environment environment;
	
	@Autowired
	private TimeReportRepository timeReportRepository;

	@ApiOperation(value = "get employee time report by given Id")
	@GetMapping("/{id}")
	public TimeReport getReportById(@PathVariable("id") int id) {
		int port = Integer.parseInt(environment.getProperty("local.server.port"));

		if (timeReportRepository.findById(id).isPresent()) {
			 TimeReport timeReport = timeReportRepository.findById(id).get();
			 timeReport.setPort(port);
			 return timeReport;
		}
		return new TimeReport();
	}

	@ApiOperation("List all availble data")
	@GetMapping("/all")
	public List<TimeReport> getAll() {
		return timeReportRepository.findAll();
	}

	@PostMapping
	public TimeReport save(@RequestBody TimeReport item) {
		return timeReportRepository.save(item);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") int id) {
		timeReportRepository.deleteById(id);
	}
	
	@PutMapping
	public TimeReport update(@RequestBody TimeReport item) {
		return timeReportRepository.save(item);
	}
}
