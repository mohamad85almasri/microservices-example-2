package com.example.fetchbasesalary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class FetchBaseSalaryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FetchBaseSalaryApplication.class, args);
	}

}
