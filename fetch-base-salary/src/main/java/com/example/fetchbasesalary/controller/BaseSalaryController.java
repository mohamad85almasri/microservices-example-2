package com.example.fetchbasesalary.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.fetchbasesalary.model.EmployeeBaseSalary;
import com.example.fetchbasesalary.repository.BaseSalaryRepository;

import io.swagger.annotations.ApiOperation;

@RequestMapping("/base")
@RestController
@EnableAutoConfiguration
public class BaseSalaryController {

	@Autowired
	private BaseSalaryRepository baseSalaryRepository;

	@ApiOperation(value = "get employee base salary by given ID")
	@GetMapping("/{id}")
	public double getBaseSalaryById(@PathVariable("id") int id) {
		if (baseSalaryRepository.findById(id).isPresent()) {
			return baseSalaryRepository.findById(id).get().getBaseSalaryPerDay();
		}
		return 0.0;
	}

	@ApiOperation("List all availble data")
	@GetMapping("/all")
	public List<EmployeeBaseSalary> getAll() {
		return baseSalaryRepository.findAll();
	}

	@PostMapping
	public EmployeeBaseSalary save(@RequestBody EmployeeBaseSalary emp) {
		return baseSalaryRepository.save(emp);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") int id) {
		baseSalaryRepository.deleteById(id);
	}
	
	@PutMapping
	public EmployeeBaseSalary update(@RequestBody EmployeeBaseSalary emp) {
		return baseSalaryRepository.save(emp);
	}
	
}
