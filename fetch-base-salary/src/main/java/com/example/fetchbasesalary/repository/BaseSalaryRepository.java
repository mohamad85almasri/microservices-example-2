package com.example.fetchbasesalary.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.fetchbasesalary.model.EmployeeBaseSalary;

public interface BaseSalaryRepository extends JpaRepository<EmployeeBaseSalary, Integer>{

}
