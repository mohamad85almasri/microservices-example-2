package com.example.fetchbasesalary.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class EmployeeBaseSalary {
	@Id
	int id;
	
	double  baseSalaryPerDay;
//	double	baseSalaryPerHour;
//	double  baseSalaryPerMonth;
}
