package com.example.employeepayslip.service;

import com.example.employeepayslip.dto.SalaryDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("calculate-salary-service")
public interface CalculateSalaryServiceClient {
    
    @GetMapping("calculate/{id}")
    public SalaryDto getSalary(@RequestParam("id") int id);
}
