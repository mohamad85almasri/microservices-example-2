package com.example.employeepayslip.service;

import com.example.employeepayslip.dto.EmployeeDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("hr-service")
public interface HrServiceClient {
    
    @GetMapping("/employee")
    public EmployeeDto getEmployee(@RequestParam("id") int id);
}
