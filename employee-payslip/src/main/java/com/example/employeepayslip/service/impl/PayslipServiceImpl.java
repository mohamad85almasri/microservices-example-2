package com.example.employeepayslip.service.impl;

import com.example.employeepayslip.dto.EmployeeDto;
import com.example.employeepayslip.dto.PayslipDto;
import com.example.employeepayslip.dto.SalaryDto;
import com.example.employeepayslip.service.CalculateSalaryServiceClient;
import com.example.employeepayslip.service.HrServiceClient;
import com.example.employeepayslip.service.PayslipService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PayslipServiceImpl implements PayslipService {
    
    private HrServiceClient hrServiceClient;

    private CalculateSalaryServiceClient calculateSalaryServiceClient;

    @Autowired
    public PayslipServiceImpl(HrServiceClient hrServiceClient,
            CalculateSalaryServiceClient calculateSalaryServiceClient) {
        this.hrServiceClient = hrServiceClient;
        this.calculateSalaryServiceClient = calculateSalaryServiceClient;
    }


    @Override
    public PayslipDto getPayslip(Integer id) {
        SalaryDto salary = calculateSalaryServiceClient.getSalary(id);
        EmployeeDto employeeDto = hrServiceClient.getEmployee(id);

        return new PayslipDto(employeeDto.getEmployeeId(), employeeDto.getFirstName(), employeeDto.getLastName(), employeeDto.getEmployeeRole(), employeeDto.getTeam(), salary.getSalary());
    }
}
