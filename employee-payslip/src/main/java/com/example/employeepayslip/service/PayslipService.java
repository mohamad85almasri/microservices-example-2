package com.example.employeepayslip.service;

import com.example.employeepayslip.dto.PayslipDto;

public interface PayslipService {
    
    public PayslipDto getPayslip(Integer id);
}
