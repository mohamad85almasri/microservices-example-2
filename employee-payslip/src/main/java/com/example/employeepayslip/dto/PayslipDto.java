package com.example.employeepayslip.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PayslipDto {
    
    private int employeeId;

    private String firstName;

    private String lastName;

    private String employeeRole;

    private String team;

    private double salary;
}
