package com.example.employeepayslip.controller;

import com.example.employeepayslip.dto.PayslipDto;
import com.example.employeepayslip.service.PayslipService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class PayslipController {

    private PayslipService payslipService;

    @Autowired
    public PayslipController(PayslipService payslipService) {
        this.payslipService = payslipService;
    }

    @GetMapping("/payslip")
    public PayslipDto getEmployeeInfo(@RequestParam("id") Integer id) {
        log.info("Returning payslip Information");
        PayslipDto payslipDto = payslipService.getPayslip(id);

        return payslipDto;
    }
}
