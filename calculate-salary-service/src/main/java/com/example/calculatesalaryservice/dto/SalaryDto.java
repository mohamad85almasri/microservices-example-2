package com.example.calculatesalaryservice.dto;


import lombok.Data;

@Data
public class SalaryDto {
	int id;
	
	TimeReport  timeReport; //separate class just to show port number
	double  payRaiseRate;
	double  baseSalaryPerDay;

	public double getSalary() {
		return timeReport.reportrdWorkingHours*(baseSalaryPerDay+payRaiseRate*baseSalaryPerDay);
	}
}
