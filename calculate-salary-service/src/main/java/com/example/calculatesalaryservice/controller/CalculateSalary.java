package com.example.calculatesalaryservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.calculatesalaryservice.dto.SalaryDto;
import com.example.calculatesalaryservice.dto.TimeReport;
import com.example.calculatesalaryservice.service.SalaryService;

@RestController
@RequestMapping("/calculate")
@EnableAutoConfiguration
public class CalculateSalary {
	
	//Map<String, Integer> uriVarialbles = new HashMap<>();

	SalaryService salaryService;

	@Autowired
	public CalculateSalary(SalaryService salaryService) {
		this.salaryService = salaryService;
	}
	
	@GetMapping("/{id}")
	public SalaryDto getById(@PathVariable("id") int id) {
		//uriVarialbles.put("id", id);
		SalaryDto salaryDto = new SalaryDto();
		salaryDto.setId(id);
		salaryDto.setTimeReport(getWorkingHours(id));
		salaryDto.setBaseSalaryPerDay(getBaseSalary(id));
		salaryDto.setPayRaiseRate(getRaiseRate(id));
		return salaryDto;
		
	}

	public TimeReport getWorkingHours(int id) {
		// ResponseEntity<TimeReport> responseEntity = new RestTemplate()
		// 		.getForEntity("http://localhost:8400/time/{id}", TimeReport.class,uriVarialbles);

		return salaryService.getWorkingHours(id);
	}
	
	public double getRaiseRate(int id) {
		// ResponseEntity<Double> responseEntity = new RestTemplate()
		// 		.getForEntity("http://localhost:8500/promotion/{id}", Double.class,uriVarialbles);

		return salaryService.getRaiseRate(id);
	}

	public double getBaseSalary(int id) {
		// ResponseEntity<Double> responseEntity = new RestTemplate()
		// 		.getForEntity("http://localhost:8600/base/{id}", Double.class,uriVarialbles);

		return salaryService.getBaseSalary(id);
	}

	


	
			
}
