package com.example.calculatesalaryservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("fetch-base-salary")
public interface BaseSalaryServiceClient {
    
    @GetMapping("base/{id}")
    public double getBaseSalary(@RequestParam("id") int id);
}
