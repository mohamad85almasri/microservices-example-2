package com.example.calculatesalaryservice.service.impl;

import com.example.calculatesalaryservice.dto.TimeReport;
import com.example.calculatesalaryservice.service.BaseSalaryServiceClient;
import com.example.calculatesalaryservice.service.PromotionLevelServiceClient;
import com.example.calculatesalaryservice.service.SalaryService;
import com.example.calculatesalaryservice.service.TimeReportServiceClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SalaryServiceImpl implements SalaryService {

    private BaseSalaryServiceClient baseSalaryServiceClient;

    private PromotionLevelServiceClient promotionLevelServiceClient;

    private TimeReportServiceClient timeReportServiceClient;
    

    @Autowired
    public SalaryServiceImpl(BaseSalaryServiceClient baseSalaryServiceClient,
            PromotionLevelServiceClient promotionLevelServiceClient, TimeReportServiceClient timeReportServiceClient) {
        this.baseSalaryServiceClient = baseSalaryServiceClient;
        this.promotionLevelServiceClient = promotionLevelServiceClient;
        this.timeReportServiceClient = timeReportServiceClient;
    }

    @Override
    public TimeReport getWorkingHours(int id) {
        return timeReportServiceClient.getWorkingHours(id);
    }

    @Override
    public double getRaiseRate(int id) {
        return promotionLevelServiceClient.getRaiseRate(id);
    }

    @Override
    public double getBaseSalary(int id) {
        return baseSalaryServiceClient.getBaseSalary(id);
    }
    
}
