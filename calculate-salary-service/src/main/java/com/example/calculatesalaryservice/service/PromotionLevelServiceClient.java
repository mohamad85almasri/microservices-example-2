package com.example.calculatesalaryservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("promotion-level-service")
public interface PromotionLevelServiceClient {

    @GetMapping("promotion/{id}")
    public double getRaiseRate(@RequestParam("id") int id);
    
}
