package com.example.calculatesalaryservice.service;

import com.example.calculatesalaryservice.dto.TimeReport;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("time-report-service")
public interface TimeReportServiceClient {

    @GetMapping("time/{id}")
    public TimeReport getWorkingHours(@RequestParam("id") int id);
    
}
