package com.example.calculatesalaryservice.service;

import com.example.calculatesalaryservice.dto.TimeReport;

public interface SalaryService {

    public TimeReport getWorkingHours(int id);

    public double getRaiseRate(int id);

    public double getBaseSalary(int id);

}
