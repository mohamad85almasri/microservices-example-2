package com.example.promotionlevelservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.promotionlevelservice.model.PayRaiseRate;
import com.example.promotionlevelservice.repository.PayRaiseRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/promotion")
public class PayRaiseRateController {

	@Autowired
	PayRaiseRepository payRaiseRepository;

	@ApiOperation(value = "get employee pay raise ate  by given Id")
	@GetMapping("/{id}")
	public double getRaiseById(@PathVariable("id") int id) {
		if (payRaiseRepository.findById(id).isPresent()) {
			return payRaiseRepository.findById(id).get().getPayRaiseRate();
		}
		return 0.0;
	}

	@ApiOperation("List all availble data")
	@GetMapping("/all")
	public List<PayRaiseRate> getAll() {
		return payRaiseRepository.findAll();
	}

}
