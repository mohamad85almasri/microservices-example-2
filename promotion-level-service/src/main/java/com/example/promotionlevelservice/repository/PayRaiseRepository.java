package com.example.promotionlevelservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.promotionlevelservice.model.PayRaiseRate;

public interface PayRaiseRepository extends JpaRepository<PayRaiseRate, Integer>{

}
