package com.example.promotionlevelservice.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
public class PayRaiseRate {

	@Id
	int id;
	
	double payRaiseRate;
	
	
}
