package com.example.promotionlevelservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class PromotionLevelServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PromotionLevelServiceApplication.class, args);
	}

}
